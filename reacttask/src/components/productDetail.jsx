import React, { useState } from "react";
import "../App.css";
import {useNavigate } from "react-router-dom";
import {useLocation } from "react-router-dom";
const Cart = (props) => {
   const {setCart}=props; 
   const navigate=useNavigate();
    const location = useLocation();
    const [proddetail,setProddetail]=useState(location.state !==null ? location.state.prod : null)
    const addProduct =()=>{
  
    setCart((prevcount)=> prevcount+1)
    }  

  return (
    <>
    {proddetail!== null ?  (<div style={{ textAlign: "center" }}>
    <span onClick={() => navigate("/")}>Back</span>
    <div className="products">
    <img src={proddetail.image} alt={proddetail.title}  height="350px"/>
    <div className="productDesc">
      <span style={{ fontWeight: 700 }}>Title : {proddetail.title}</span>
      <span style={{ fontWeight: 500 }}>Description : {proddetail.description}</span>
      <span  style={{ fontWeight: 700 }}>Category : {proddetail.category}</span>
      <span>Rating : {proddetail.rating.rate}</span>
      <span>Count : {proddetail.rating.count}</span>
      <span>Price : ₹ {proddetail.price}</span>
    </div>
  
      <button className="add" onClick={() =>addProduct() }>
       Add to Cart
      </button>
</div>
</div>) :
   (<div style={{ textAlign: "center" }}>
      <span onClick={() => navigate("/")}>NO PRODUCT SELECTED</span>
     
 </div>)}
 </>
  );
};

export default Cart;
