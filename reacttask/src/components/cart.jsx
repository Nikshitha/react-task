import React from "react";


const Cart = (props) => {
  const {cart}=props; 
  return (
    <div style={{ textAlign: "center" }}>
      <span style={{ fontSize: 30 }}>My Cart({cart})</span>
      
    </div>
  );
};

export default Cart;
