
import React,{useEffect,useState} from "react";
import "../App.css";
import axios from "axios";
import {useNavigate } from "react-router-dom";
const Home = (props) => {
  const {search}=props; 
    const navigate=useNavigate();
    const [categories,setCategories] = useState([]);
    const [product,setProduct] = useState([]);
   const [displaycat,setDisplaycat] = useState(true);
    useEffect(() => {
        fetch('https://fakestoreapi.com/products/categories')
        .then(res=>res.json())
        .then(json=> setCategories(json))
  
      },[])
const displayProducts =(cat)=>{
    setDisplaycat(false)
    axios.get(`https://fakestoreapi.com/products/category/${cat}`).then((response) => {
        setProduct(response.data);
      });
}
  
  return (
    displaycat ? 
    <div style={{ textAlign: "center" }}>

      {categories.filter((cat)=>{
          if (search=="") {
            return cat;
          }else if (cat.toLowerCase().includes(search.toLowerCase())){
            return cat;
          }
        }) .map((cat,index)=>{return( <div className="products" key={index} onClick={()=>displayProducts(cat)}>
        <span style={{ fontWeight: 700 }}>{cat}</span>
      </div>)  })} </div>
        :
        <div style={{ textAlign: "center" }}>
              <span onClick={()=>    setDisplaycat(true)}>Back</span>
        { product.filter((prod)=>{
          if (search=="") {
            return prod;
          }else if (Object.keys(prod).some(key=>prod[key].toString().toLowerCase().includes(search.toLowerCase())) ){
            return prod;
          }  }) .map((prod,index)=>{return(

         <div className="products"  key={index} onClick={() => navigate("/productDetails",{ state: { prod } })}>
        <img src={prod.image} alt={prod.title}  height="350px"/>
        <div className="productDesc">
          <span style={{ fontWeight: 700 }}>{prod.title}</span>
          <span>₹ {prod.price}</span>
        </div>
     
      </div>
   ) })}
  </div>
  );
};

export default Home;