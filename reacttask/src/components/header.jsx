import { Link, Outlet } from "react-router-dom";
import "../App.css";
const Header = (props) => {

const {cart,setSearch}=props; 
  return (
    <div><div>
    <span className="header">Cart</span>
    <ul className="nav">
      <li>
        <Link   className="prod" to="/">Home</Link>
      </li>
      <li>
        <Link   className="prod" to="/productDetails">Product Details</Link>
      </li>
      <li>
        <input type="search" placeholder="Search..." onChangeCapture={(e)=>setSearch(e.target.value)} ></input>
      </li>
      <li >
        <Link  className="prod" to="/cart">Cart({cart}) </Link>
      </li>
      <li>
        <Link   className="prod" to="/profile">Profile</Link>
      </li>
    </ul>
  </div>
  <Outlet/>
  </div>
  );
};

export default Header;
