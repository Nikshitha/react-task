import React, { useState } from "react";
import "./App.css";
import Header from "./components/header";
import Home from "./components/home";
import Cart from "./components/cart";
import Profile from "./components/profile";
import ProductDetails from "./components/productDetail";
import {
  createBrowserRouter,
  createRoutesFromElements,
  Route,
  RouterProvider
} from "react-router-dom";


function App() {
  const [cart,setCart] = useState(0);
  const [search,setSearch] = useState("");
  const router = createBrowserRouter(
    createRoutesFromElements(
      <Route path="/" element={<Header  cart={cart}  setSearch={setSearch} />} >
      <Route path="/" element={<Home search={search} />} />
      <Route  path="/productDetails" element={<ProductDetails  setCart={setCart}/>}/>
      <Route  path="/cart" element={<Cart cart={cart} />}/>
      <Route  path="/profile" element={<Profile  />}/>
      </Route>
  
    )
  )
 return  ( <div className="App">
      <RouterProvider router={router}/>
    </div>
 )
  
}

export default App;

